<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;

class ContactController extends Controller
{
    /**
     * @Route("/contact/form")
     */
    public function formAction()
    {
        return $this->render('contact/form.html.twig', []);
    }

     /**
     * @Route("/contact/list")
     */
    public function listAction()
    {
        $contact = $this->get('app.model.contact');
        $contacts = $contact->getAll();
        return $this->render('contact/list.html.twig', ['contacts' => $contacts]);
    }


    /**
     * @Route("/contact/post")
     * @Method({"POST"})
     */
    public function postAction(Request $request)
    {
        $contact = $this->get('app.model.contact');
        $id = $contact->save($request->request->all());
        return new JsonResponse(['id' => $id, 'messages' => $contact->getValidationMessages()]);
    }

}