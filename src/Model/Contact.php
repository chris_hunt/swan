<?php
namespace Model;

use Respect\Validation\Validator as v;

class Contact
{
    private $storageAdapter;

    private $validator;

    private $validationMessages = [];

    public function __construct(\Adapter\StorageAdapterInterface $storageAdapter) 
    {
        $this->storageAdapter = $storageAdapter;
    }

    public function save($contact)
    {
        if ($this->validate($contact)) {
            $id = $this->storageAdapter->insertContact($contact['name'], $contact['email'], $contact['phone']);
            return $id;
        } else {
            return null;
        }
    }

    protected function getValidators()
    {
        return [
            "name" => [
                [
                    "validator" => v::length(4,100),
                    "message" => "Please enter a name between 4 and 100 characters"
                ]
            ],
            "email" => [
                [
                    "validator" => v::email(),
                    "message" => "Please enter a valid email address"
                ]
            ],
            "phone" => [
                [
                    "validator" => v::regex('/^[0-9- ]+$/D'),
                    "message" => "Please ensure your phone number contains only numbers, hyphens and spaces"
                ]
            ]
        ];
    }

    public function validate($contact)
    {
        $valid = true;
        $this->validationMessages = [];

        $validators = $this->getValidators();
        foreach ($validators as $field=>$validatorList) {
            foreach ($validatorList as $validator) {
                if (!$validator['validator']->validate($contact[$field])) {
                    $valid = false;
                    $this->validationMessages[$field][] = $validator["message"];
                }
            }            
        }

        return $valid;
    }

    public function getValidationMessages()
    {
        return $this->validationMessages;
    }

    public function getAll()
    {
        return $this->storageAdapter->getContacts();
    }
}