<?php
namespace Adapter\Storage;

class MySql implements \Adapter\StorageAdapterInterface
{

    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function insertContact($name, $email, $phone)
    {
        $this->db
            ->prepare('INSERT INTO contact(name, email, phone) VALUES (?,?,?)')
            ->execute([$name, $email, $phone]);
        return $this->db->lastInsertId();
    }

    public function getContact($id)
    {
        $stmt = $this->db->prepare('SELECT * FROM contact WHERE id = ?');
        $stmt->execute([$id]);
        $contact = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $contact;
    }

    public function getContacts()
    {
        $stmt = $this->db->prepare('SELECT * FROM contact');
        $stmt->execute();
        $contacts = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $contacts;
    }
}