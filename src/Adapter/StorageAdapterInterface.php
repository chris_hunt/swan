<?php

namespace Adapter;

interface StorageAdapterInterface
{
    public function insertContact($name, $email, $phone);
    public function getContact($id);
    public function getContacts();
}