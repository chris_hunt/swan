Chris Hunt - Swan Demo
======================

**Install**

Run `composer update` from root folder.

**Running**

Create database and contacts table with
```
CREATE DATABASE  IF NOT EXISTS `swan` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `swan`;

DROP TABLE IF EXISTS `contact`;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
```

Edit database params in `/app/config/parameters,yml`.

Run `php bin/console server:run` from root. 

From there, contact entry form available at `/contact/form`.

Contact list available at `/contact/list`.